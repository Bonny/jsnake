package jsnake;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import javax.swing.*;

public final class GameFrame extends JFrame {

    private boolean sinistra, destra, su, giu, cibo, isGame;
    private final int CELLE = 35;
    private int dx, dy, i, j, n, dxCibo, dyCibo, punteggio;
    private int vCol[], vRow[];
    private Scheduler sc;
    private GameLabelSnake matrix[][];
    private JLabel record, punti;
    private JButton btnStart;
    private JComboBox comboLivel;

    public GameFrame() {
        super("JSnake");
        vCol = new int[CELLE];
        vRow = new int[CELLE];
        matrix = new GameLabelSnake[CELLE][CELLE];
        JPanel pl = new JPanel();
        pl.setLayout(new FlowLayout());
        pl.setBorder(BorderFactory.createEtchedBorder());
        comboLivel = new JComboBox();
        comboLivel.setEditable(false);
        comboLivel.addItem("Facile");
        comboLivel.addItem("Medio");
        comboLivel.addItem("Difficile");
        btnStart = new JButton("Start Game");
        punti = new JLabel("--");
        record = new JLabel(String.valueOf(readRecord()));
        JLabel iconPunti = new JLabel();
        iconPunti.setIcon(new ImageIcon(getClass().getResource("/images/star.png")));
        iconPunti.setBorder(BorderFactory.createEmptyBorder(0, 45, 0, 5));
        JLabel iconRecord = new JLabel();
        iconRecord.setIcon(new ImageIcon(getClass().getResource("/images/em.png")));
        iconRecord.setBorder(BorderFactory.createEmptyBorder(0, 45, 0, 5));
        pl.add(comboLivel);
        pl.add(btnStart);
        pl.add(iconPunti);
        pl.add(punti);
        pl.add(iconRecord);
        pl.add(record);
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints lim = new GridBagConstraints();
        JPanel p = new JPanel();
        p.setBorder(BorderFactory.createLineBorder(new Color(192, 88, 0), 3));
        p.setLayout(layout);
        for (i = 0; i < CELLE; i++) {
            lim.gridy = i;
            for (j = 0; j < CELLE; j++) {
                matrix[i][j] = new GameLabelSnake();
                matrix[i][j].setSfondo();
                lim.gridx = j;
                layout.setConstraints(matrix[i][j], lim);
                p.add(matrix[i][j]);
            }
        }

        GridBagLayout layout2 = new GridBagLayout();
        GridBagConstraints lim2 = new GridBagConstraints();
        Container ct = this.getContentPane();
        ct.setLayout(layout2);
        lim2.gridx = lim2.gridy = 0;
        lim2.insets.bottom = 5;
        lim2.fill = GridBagConstraints.BOTH;
        layout2.setConstraints(pl, lim2);
        ct.add(pl);
        lim2.gridx = 0;
        lim2.gridy = 1;
        layout2.setConstraints(p, lim2);
        ct.add(p);
        ct.setBackground(Color.GRAY);

        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    if ((su || giu)) {
                        sinistra = true;
                        destra = giu = su = false;
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    if (su || giu) {
                        destra = true;
                        sinistra = giu = su = false;
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                    if (destra || sinistra) {
                        su = true;
                        giu = destra = sinistra = false;
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    if (destra || sinistra) {
                        giu = true;
                        su = destra = sinistra = false;
                    }
                }

            }
        });

        btnStart.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                requestFocusInWindow();
                int time = 0;
                if (!isGame) {
                    switch (comboLivel.getSelectedIndex()) {
                        case 0:
                            time = 120;
                            break;
                        case 1:
                            time = 90;
                            break;
                        case 2:
                            time = 60;
                            break;
                    }
                    initGriglia();
                    sc = null;
                    sc = new Scheduler(time);
                    sc.start();
                    isGame = true;
                }
            }
        });

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);
        this.setBounds((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2) - 500 / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2) - 580 / 2,
                500, 580);
    }

    public void initGriglia() {

        isGame = cibo = sinistra = su = giu = cibo = false;
        destra = true;
        n = 14;
        punteggio = dxCibo = dyCibo = 0;
        for (i = 0; i < CELLE; i++) {
            vCol[i] = vRow[i] = 0;
            for (j = 0; j < CELLE; j++) {
                matrix[i][j].setSfondo();
            }
        }
        int t = CELLE / 2;
        dx = dy = t;
        matrix[t][t].setSnakeTesta();

        for (i = 0; i < n; i++) {
            vRow[i] = t;
            vCol[i] = t - i;
            if (i > 0) {
                matrix[t][t - i].setSnake();
            }
        }
        punti.setText("0");
        record.setText(String.valueOf(readRecord()));
    }

    public void generaCibo() {
        boolean flag = false;
        int x = 0, y = 0, k;
        while (!flag) {
            k = 0;
            x = (int) (Math.random() * CELLE);
            y = (int) (Math.random() * CELLE);
            //System.out.println(x + "\t" + y);
            while (k < n) {
                if (vRow[k] == x && vCol[k] == y) {
                    k = n + 1;
                    flag = false;
                } else {
                    k++;
                    flag = true;
                }
            }
        }
        matrix[x][y].setCibo();
        dxCibo = x;
        dyCibo = y;
    }

    private int readRecord() {
        int t = 0;
        try {
            try (BufferedReader fin = new BufferedReader(
                            new FileReader(getClass().getResource("/file/foo.txt").getFile()))) {
                t = Integer.parseInt(fin.readLine());
            }
        } catch (IOException ex) {
            System.out.println("ERRORE: " + ex.getMessage());
        }
        return t;
    }

    private void writeNewRecord(int record) {
        try {
            try (PrintWriter fout = new PrintWriter(
                            new FileWriter(getClass().getResource("/file/foo.txt").getFile()))) {
                fout.println(record);
                //fout.flush();
                fout.close();
            }
        } catch (IOException ex) {
            System.out.println("ERRORE: " + ex.getMessage());
        }

    }

    class Scheduler implements Runnable {

        private Thread thread;
        private int timeStamp;

        public Scheduler(int timeStamp) {
            this.timeStamp = timeStamp;
            this.thread = new Thread(this);
        }

        public void start() {
            this.thread.start();
        }

        public void stop() {
            int p = Integer.valueOf(punti.getText());
            if (p > readRecord()) {
                writeNewRecord(p);
                JOptionPane.showMessageDialog(null,
                        "Hai battuto il record!!\nTotalizzando " + p + " punti.", "Messaggio",
                        JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null,
                        "Game Over!!\nPunti totalizzati " + p + " punti.", "Messaggio",
                        JOptionPane.INFORMATION_MESSAGE);
            }
            for (i = 0; i < CELLE; i++) {
                vCol[i] = vRow[i] = 0;
                for (j = 0; j < CELLE; j++) {
                    matrix[i][j].setSfondo();
                }
            }
            this.thread.stop();
            this.thread.destroy();
        }

        @Override
        public void run() {

            boolean go = true, flag;

            while (go) {

                if (!cibo) {
                    generaCibo();
                    cibo = true;
                }

                if (dxCibo == dx && dyCibo == dy) {
                    punti.setText(String.valueOf(++punteggio));
                    cibo = false;
                    n++;
                }

                matrix[vRow[n - 1]][vCol[n - 1]].setSfondo();
                for (i = n - 1; i > 0; i--) {
                    vRow[i] = vRow[i - 1];
                    vCol[i] = vCol[i - 1];
                }

                vRow[1] = dx;
                vCol[1] = dy;
                matrix[dx][dy].setSnake();

                if (destra) {
                    dy = (dy + 1) % CELLE;
                } else if (sinistra) {
                    dy = (dy > 0) ? dy - 1 : CELLE - 1;
                } else if (su) {
                    dx = (dx > 0) ? dx - 1 : CELLE - 1;
                } else if (giu) {
                    dx = (dx + 1) % CELLE;
                }
                vRow[0] = dx;
                vCol[0] = dy;
                matrix[dx][dy].setSnakeTesta();

                i = 1;
                flag = false;
                while (i < n && !flag) {
                    if (vRow[0] == vRow[i] && vCol[0] == vCol[i]) {
                        flag = true;
                    } else {
                        i++;
                    }
                }

                if (flag) {
                    go = false;
                }

                try {
                    Thread.sleep(timeStamp);
                } catch (InterruptedException ex) {
                    System.err.println("ERRORE:\n" + ex.getMessage());
                }
            }
            isGame = false;
            stop();
        }
    }
}
