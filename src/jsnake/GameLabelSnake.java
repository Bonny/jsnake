package jsnake;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class GameLabelSnake extends JLabel {
    
    private ImageIcon imgSnake, imgSfondo, imgSnakeTesta, imgCibo;
    
    public GameLabelSnake() {
        imgSfondo = new ImageIcon(getClass().getResource("/images/img_bianca.png"));
        imgSnake = new ImageIcon(getClass().getResource("/images/img_verde.png"));
        imgSnakeTesta = new ImageIcon(getClass().getResource("/images/img_testa.png"));
        imgCibo = new ImageIcon(getClass().getResource("/images/cibo.png"));
    }
    
    public void setSfondo() {
        this.setIcon(imgSfondo);
    }
    
    public void setSnake() {
        this.setIcon(imgSnake);
    }
    
    public void setSnakeTesta() {
        this.setIcon(imgSnakeTesta);
    }
    
    public void setCibo() {
        this.setIcon(imgCibo);
    }
}
